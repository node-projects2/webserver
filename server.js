const express = require('express');
const app = express();
var hbs = require('hbs');
require('./hbs/helpers');

const port = process.env.PORT || 3000;

// Para que nodemon escuche otras extensiones aparte de js
// en la consola => nodemon server -e js,hbs,html,css

app.use(express.static(__dirname + '/public'));


// Express hbs
hbs.registerPartials(__dirname + '/views/parciales', function (err) { });
app.set('view engine', 'hbs');

app.get('/', (req, res) => {


    res.render('Home', {
        nombre: 'david'
    });
});
// app.get('/data', (req, res) => {

//     res.send('Hola data')
// });

app.get('/about', (req, res) => {


    res.render('about', {
        nombre: 'David'
    });
});

app.listen(port, () => {
    console.log(`Escuchando peticiones en el puerto ${port}`);
});

// para ejecutar el proceso que hay en los scripts del package.json
// para el start, vale con: npm start
// para el de nodemon, hay que poner: npm run nodemon